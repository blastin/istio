# Arquitetura de Segurança em ambiente kubernetes

## Iniciando Projeto

[script](init.sh)

## Garantir criptografia de comunicação interna e externa

    TLS, MUTUAL TLS


### Criando certificado de criptografia auto gerado

~~~sh

export DOMAIN_NAME=knin.project.br

openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=knin.project.br Inc./CN=knin.project.br' -keyout $DOMAIN_NAME.key -out $DOMAIN_NAME.crt

openssl req -out oi.$DOMAIN_NAME.csr -newkey rsa:2048 -nodes -keyout oi.$DOMAIN_NAME.key -subj "/CN=oi.knin.project.br/O=oi mundo from knin.project.br"

openssl x509 -req -sha256 -days 365 -CA $DOMAIN_NAME.crt -CAkey $DOMAIN_NAME.key -set_serial 0 -in oi.$DOMAIN_NAME.csr -out oi.$DOMAIN_NAME.crt

~~~

### Criando Secret no kubernetes

~~~sh
kubectl create -n istio-system secret tls knin-project-br-secret \
  --cert=oi.knin.project.br.crt  \
  --key=oi.knin.project.br.key
~~~

~~~yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gateway-backend
  namespace: infra
spec:
  selector:
    istio: ingressgateway
  servers:
    - port:
        number: 443
        name: https
        protocol: HTTPS
      hosts:
        - oi.knin.project.br
      tls:
        mode: SIMPLE
        credentialName: knin-project-br-secret
~~~

Preciso estudar sobre TLS e Mutal TLS

### Configurar tráfico de rotas para componentes de borda

    Ingress, Virtual Service (Istio Gateway),

~~~yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: backend
  namespace: infra
spec:
  hosts:
    - oi.knin.project.br
  gateways:
    - infra/gateway-backend
  http:
    - match:
        - uri:
            exact: /mensagens
      headers:
        request:
            set:
                type-component: "borda" 
      route:
        - destination:
            host: componente.backend.svc.cluster.local
            port:
              number: 8080
~~~

Chamada

~~~sh
export INGRESS_IP=$(kubectl get svc istio-ingressgateway -n istio-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
curl -H "Host:oi.knin.project.br" --resolve "oi.knin.project.br:443:$INGRESS_IP" --cacert knin.project.br.crt "https://oi.knin.project.br:443"
~~~

### Trabalhando Com Mutual TLS

~~~sh
openssl req -out client.knin.project.br.csr -newkey rsa:2048 -nodes -keyout client.knin.project.br.key -subj "/CN=client.knin.project.br/O=client organization"

openssl x509 -req -sha256 -days 365 -CA knin.project.br.crt -CAkey knin.project.br.key -set_serial 1 -in client.knin.project.br.csr -out client.knin.project.br.crt

~~~

### Criando Secret Generic no kubernetes

~~~sh
kubectl create -n istio-system secret generic knin-project-br-secret \
--from-file=tls.key=oi.knin.project.br.key \
--from-file=tls.crt=oi.knin.project.br.crt \
--from-file=ca.crt=knin.project.br.crt
~~~


~~~yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gateway-backend
  namespace: infra
spec:
  selector:
    istio: ingressgateway
  servers:
    - port:
        number: 443
        name: https
        protocol: HTTPS
      hosts:
        - oi.knin.project.br
      tls:
        mode: MUTUAL
        credentialName: knin-project-br-secret
~~~

Chamada

~~~json
curl -v -H "Host:oi.knin.project.br" --resolve "oi.knin.project.br:443:$INGRESS_IP" --cacert knin.project.br.crt --cert client.knin.project.br.crt --key client.knin.project.br.key "https://oi.knin.project.br:443/mensagens"
~~~

### Criando certificado de forma automatizada por cert-manager

Necessita de um dominio proprio

## Assegurar autenticação e autorização de acesso

(JWT, Oauth2)


JWT - Json Web Token

Padrão aberto que define o tráfego de informações via web de forma segura entre duas partes


Dois Cenários de utilização


### Autorização

Tráfego de assinatura para autorização de acesso a serviços

* Login com Open ID
* Level de acesso a recursos
* Políticas de acesso a serviços
  
### Troca de Mensagem

Troca de informações entre partes com assinatura de chave pública e privada


### Estrutura

~~~json
{
  "header" : {
    "alg": "HS256",
    "typ": "JWT"
  },
  "payload" {
    "sub": "5bhu8skpffmeb8e1a4hfmdcvgi", // AGREGA LEVEL DE ACESSO A RECURSOS E POLITICAS DE ACESSO A SERVIÇOS
    "issuer": "Jeff Lisboa",
    "exp": 34456450,
    "aud": ["oi.knin.project.br"]
  }

}
~~~

Cadeia de caracteres

~~~c
token = criptografia
(
    base64URL(header) + '.' + base64URL(payload)
  , secret
)
~~~

É importante frisar que não se deve trafegar dados sensíveis entre as partes, pois tanto o header quanto payload são decodificados facilmente.

### Configurando Service Mesh

1. Configurando lista de servidores que validam informações de JWT

~~~yaml
apiVersion: security.istio.io/v1beta1
kind: RequestAuthentication
metadata:
  name: authentication-jwt
  namespace: backend
spec:
  selector:
    matchLabels:
      type: borda
  jwtRules:
    - issuer: "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_ro61niJ6m" # EMISSOR DA JWT
      jwksUri: https://cognito-idp.us-east-1.amazonaws.com/us-east-1_ro61niJ6m/.well-known/jwks.json # CHAVE PUBLICA PARA VERIFICAR VALIDADE DO TOKEN
      audiences:
        - oi.knin.project.br # PUBLICO COM PERMISSÃO DE ACESSO
      fromHeaders: # CABEÇALHOS ESPERADOS
        - name: Token
          prefix: "Bearer " 
~~~

2. Configurando Politicas de autorização para acesso a recursos ou serviços

~~~yaml
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: authorization-jwt
  namespace: backend
spec:
  selector:
    matchLabels:
      type: borda
  action: ALLOW
  rules:
  - from:
    - source:
        principals: ["*"]
    to:
    - operation:
        methods: ["GET"]
        paths: ["/mensagens"]
    when:
    - key: request.auth.claims[sub]
      values: ["5bhu8skpffmeb8e1a4hfmdcvgi"]
~~~ 

* Caso AuthorizationPolicy não seja aplicada, o controle de acesso, não será realizado

---

## Tráfego Externo (Egress Traffic)

* Configurar politicas de acesso a serviços externos

---

## Federação de Identidade

### Objetivo

Centralizar e gerenciar autenticação de usuários para ingresso a serviços e recursos

### Descrição do Keycloak

 Provedor de identidade open source que permite entrada única e gerenciamento de acesso de grupo de usuários por meios de regras pré estabelecidas
 
### Como Provisionar Keycloak

### Configurando Keycloak

1. Criar Realm
2. Criar Client
3. Configurar Rules/Group
4. Adicionar Usuário

### Fase de Teste Keycloak

Considerando que docker esteja instalado na maquina

Container Ubuntu

~~~sh
docker run -it --rm ubuntu
~~~

Iniciando Fase de Teste

~~~sh
apt update && apt upgrade -y && apt install jq curl -y

CLIENT_SECRET="lVLNJqHfMm4W2Q6TeMGj1di9pE4SrFyH"
CLIENT_ID="cliente"
REALM="teste"

HOST_KEYCLOAK="localhost:8080/realms/${REALM}"

URL_TOKEN="$(curl -v --location ${HOST_KEYCLOAK}/.well-known/openid-configuration | jq '.token_endpoint' | sed 's/\"//g')"

ACCESS_TOKEN="$(curl -v --location "${URL_TOKEN}" \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode "client_id=${CLIENT_ID}" \
--data-urlencode 'grant_type=client_credentials' \
--data-urlencode "client_secret=${CLIENT_SECRET}" \
| jq '.access_token' )"

[[ $ACCESS_TOKEN =~ \w+ ]] && echo "sucesso" ||   echo "Não foi possivel recuperar Token"

~~~