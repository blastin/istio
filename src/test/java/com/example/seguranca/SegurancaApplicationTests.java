package com.example.seguranca;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@AutoConfigureWebTestClient
class SegurancaApplicationTests {

    @Autowired
    private WebTestClient webClient;

    @ParameterizedTest
    @ValueSource(strings = {"liveness", "readiness"})
    void healthCheck(final String value) {
        webClient
                .get()
                .uri("/actuator/health/".concat(value))
                .exchange()
                .expectStatus()
                .isOk();
    }
}
