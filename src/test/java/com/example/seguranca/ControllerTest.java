package com.example.seguranca;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@AutoConfigureWebTestClient
class ControllerTest {

    @Autowired
    private WebTestClient webClient;

    @Test
    void mensagem() {
        webClient
                .get()
                .uri("/mensagens")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("valor", "Oi Mundo");
    }

}