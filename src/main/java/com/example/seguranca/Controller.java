package com.example.seguranca;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/mensagens")
class Controller {

    @GetMapping
    public Mono<Mensagem> obter() {
        return Mono.just(new Mensagem());
    }

    static final class Mensagem {

        public String getValor() {
            return "Oi Mundo";
        }

    }
}
