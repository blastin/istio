#!/bin/bash


DOMAIN_NAME=knin.project.br

mkdir cacert

cd cacert

openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=knin.project.br Inc./CN=knin.project.br' -keyout $DOMAIN_NAME.key -out $DOMAIN_NAME.crt

openssl req -out oi.$DOMAIN_NAME.csr -newkey rsa:2048 -nodes -keyout oi.$DOMAIN_NAME.key -subj "/CN=oi.knin.project.br/O=oi mundo from knin.project.br"

openssl x509 -req -sha256 -days 365 -CA $DOMAIN_NAME.crt -CAkey $DOMAIN_NAME.key -set_serial 0 -in oi.$DOMAIN_NAME.csr -out oi.$DOMAIN_NAME.crt

openssl req -out client.knin.project.br.csr -newkey rsa:2048 -nodes -keyout client.knin.project.br.key -subj "/CN=client.knin.project.br/O=client organization"

openssl x509 -req -sha256 -days 365 -CA knin.project.br.crt -CAkey knin.project.br.key -set_serial 1 -in client.knin.project.br.csr -out client.knin.project.br.crt

kubectl create -n istio-system secret generic knin-project-br-secret \
--from-file=tls.key=oi.knin.project.br.key \
--from-file=tls.crt=oi.knin.project.br.crt \
--from-file=ca.crt=knin.project.br.crt

cd ..

kubectl create namespace infra

kubectl apply -f src/main/resources/gateway.yaml

kubectl apply -f src/main/resources/authentication.yaml

image="$(docker images echo:1.0 -aq)"

if [ -z "$image" ] 
then
    eval $(minikube docker-env) 
    ./mvnw clean spring-boot:build-image
fi

kubectl create namespace backend

kubectl label namespace backend istio-injection=enabled

kubectl apply -f src/main/resources/deployment.yaml

AMAZON_COGNITO_DOMAIN="pocoidc1"

COGNITO_REGION="us-east-1"

ACCESS_TOKEN=$(curl --location --request POST "https://${AMAZON_COGNITO_DOMAIN}.auth.${COGNITO_REGION}.amazoncognito.com/oauth2/token" \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Cookie: XSRF-TOKEN=babfb309-6e27-48af-b529-4f623efca2d5' \
--data-urlencode 'client_id=5bhu8skpffmeb8e1a4hfmdcvgi' \
--data-urlencode 'grant_type=client_credentials' \
--data-urlencode 'client_secret=1s8lltp3nrat4gqgh9jg6849cpa2uh4dmhrf9qsq0p51kmqmf647' \
| jq '.access_token')

TOKEN="Bearer $ACCESS_TOKEN"

INGRESS_IP=$(kubectl get svc istio-ingressgateway -n istio-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

ENDPOINT="mensagens"

SLEEP_TIME=4

ATTEMPTS_HEALTH_CHECK=4

status=0

attempts=0

while test "$status" -ne 200 -a "$attempts" -lt "${ATTEMPTS_HEALTH_CHECK}"
do

    status="$(curl -s -o /dev/null -w "%{http_code}" -H "Authorization:$TOKEN" -H 'Host:oi.knin.project.br' --resolve "oi.knin.project.br:443:$INGRESS_IP" \
--cacert cacert/knin.project.br.crt --cert cacert/client.knin.project.br.crt --key cacert/client.knin.project.br.key -k "https://oi.knin.project.br:443/$ENDPOINT")"

    attempts=$((attempts + 1))

    echo "attempts : [$((ATTEMPTS_HEALTH_CHECK - attempts))], status : [$status] health check ..."

    sleep "${SLEEP_TIME:=0}"

done

 if [ "$attempts" -lt "${ATTEMPTS_HEALTH_CHECK}" ]
 then
      
    echo "RESULTADO"

     curl -v \
     -H "Authorization:$TOKEN" \
     -H "Host:oi.knin.project.br" \
     --resolve "oi.knin.project.br:443:$INGRESS_IP" \
     --cacert cacert/knin.project.br.crt \
     --cert cacert/client.knin.project.br.crt \
     --key cacert/client.knin.project.br.key \
     "https://oi.knin.project.br:443/mensagens"

     echo "\nFIM"
     
else
    echo "NÃO FOI POSSÍVEL COMUNICAR COM COMPONENTE"
fi

kubectl delete namespace infra

kubectl delete namespace backend

kubectl delete -n istio-system secret knin-project-br-secret

rm -rf cacert